<?php

namespace Drupal\slack_logger\Logger;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\slack\Slack;
use Drupal\slack_logger\DomainManager;
use Psr\Log\LoggerInterface;

/**
 * SlackLogger controller.
 */
class SlackLogger implements LoggerInterface {
  use RfcLoggerTrait;
  use StringTranslationTrait;

  /**
   * Domain manager service.
   *
   * @var \Drupal\slack_logger\DomainManager
   */
  protected $domainManager;

  /**
   * Slack service.
   *
   * @var \Drupal\slack\Slack
   */
  protected $slack;

  /**
   * SlackLogger constructor.
   *
   * @param \Drupal\slack_logger\DomainManager $domain_manager
   *   Domain manager service.
   * @param \Drupal\slack\Slack $slack
   *   Slack service.
   */
  public function __construct(DomainManager $domain_manager, Slack $slack) {
    $this->domainManager = $domain_manager;
    $this->slack = $slack;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    $minimumLevel = $this->domainManager->getConfig('level') ?? RfcLogLevel::ERROR;

    if ($context['channel'] === "slack" || $level > (int) $minimumLevel) {
      return;
    }

    $is_allowed = $this->domainManager->isCurrentDomainAllowedToNotify();

    if ($is_allowed) {
      $messageToSend = $context['channel'] === 'php' ? $this->prepareMessage($context) : $this->prepareMessage($context, $message);
      $this->slack->sendMessage($messageToSend);
    }
  }

  /**
   * Prepare the message that will be send to Slack.
   *
   * @param array $context
   *   Logger context.
   * @param string|null $message
   *   Message text.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Translatable markup.
   */
  protected function prepareMessage(array $context, $message = NULL) {
    if (!empty($message)) {
      $context += ['@request_link' => $context['request_uri']];
      $output = $this->t($message . ' Triggered on page <a href="@request_link">@request_link</a>.', $context);
    }
    else {
      $output = $this->t('@message in %function (line %line of %file) on page <a href="@request_link">@request_link</a>.', [
        '@message' => $context['@message'],
        '%function' => $context['%function'],
        '%line' => $context['%line'],
        '%file' => $context['%file'],
        '@request_link' => $context['request_uri'],
      ]);
    }

    return $output;
  }

}
