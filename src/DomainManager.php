<?php

namespace Drupal\slack_logger;

use Drupal\Core\Config\ConfigManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DomainManager.
 */
class DomainManager {

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Allowed domains.
   *
   * @var array
   */
  protected $allowedDomains = [];

  /**
   * Constructs a new DomainManager object.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   Config manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request service.
   */
  public function __construct(ConfigManagerInterface $config_manager, RequestStack $request) {
    $this->configManager = $config_manager;
    $this->request = $request;
    $this->setAllowedDomains();
  }

  /**
   * Check if the current domain is the one of the allowed in config.
   *
   * @return bool
   *   True if the domain is allowed, false otherwise.
   */
  public function isCurrentDomainAllowedToNotify() {
    return $this->inAllowedDomains($this->getCurrentDomain());
  }

  /**
   * Get current domain name (host).
   *
   * @return string
   *   Domain name.
   */
  public function getCurrentDomain() {
    $request = $this->request->getCurrentRequest();

    return empty($request) ? '' : $request->getHost();
  }

  /**
   * Check for allowed domain.
   *
   * @param string $domain_name
   *   Name of the domain to be checked.
   *
   * @return bool
   *   Return true if domain is in allowed domains list.
   */
  public function inAllowedDomains($domain_name) {
    return in_array($domain_name, $this->getAllowedDomains());
  }

  /**
   * Getter for Slack Logger config.
   *
   * @param string $config_name
   *   Config value key.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Config.
   */
  public function getConfig($config_name = 'domains') {
    return $this->configManager->getConfigFactory()->get('slack_logger.config')->get($config_name);
  }

  /**
   * Getter for allowed domains.
   *
   * @return array
   *   List of domains.
   */
  public function getAllowedDomains() {
    return $this->allowedDomains;
  }

  /**
   * Setter for allowed domains.
   */
  protected function setAllowedDomains() {
    $allowedDomains = [];
    if (!empty($this->getConfig())) {
      $allowedDomains = $this->parseDomainsFromString($this->getConfig());
    }

    $this->allowedDomains = $allowedDomains;
  }

  /**
   * Converts string to array of domains.
   *
   * @param string $text
   *   String containg the domain names.
   *
   * @return array
   *   List of domains.
   */
  protected function parseDomainsFromString($text) {
    return preg_split('/\s+/', $text, -1, PREG_SPLIT_NO_EMPTY);
  }

}
