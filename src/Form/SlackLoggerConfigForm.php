<?php

namespace Drupal\slack_logger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Class SlackLoggerConfigForm.
 */
class SlackLoggerConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'slack_logger.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slack_logger_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('slack_logger.config');

    $form['level'] = [
      '#type' => 'select',
      '#title' => $this->t('Logging severity levels'),
      '#options' => RfcLogLevel::getLevels(),
      '#default_value' => $config->get('level') ?? RfcLogLevel::ERROR,
      '#description' => $this->t('All errors with level equal or below that value will be included'),
    ];

    $form['domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Domains'),
      '#description' => $this->t('Slack logging will be only executed for the domains provided on this list. Add domain/subdomain only: example.com, wwww.example.com. Each domain should be added in new line.'),
      '#default_value' => $config->get('domains'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('slack_logger.config')
      ->set('domains', $form_state->getValue('domains'))
      ->set('level', $form_state->getValue('level'))
      ->save();
  }

}
