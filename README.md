# Slack Logger

This module allows to send error logs to your configured Slack channel.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/slack_logger).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/slack_logger).


## Requirements

Slack Logger module extend the functionality of the
[Slack module](https://www.drupal.org/project/slack).
To use this module, you need to download/install Slack module first.


## Installation

1. Extract the tar.gz into your 'modules' directory or get it
   via composer: `composer require drupal/slack_logger`.
1. Go to "Extend" after successfully login into admin.
1. Enable the module at `administration >> extend`.


## Configuration

1. Go to `/admin/config/services/slack/slack_logger`.
1. Select a minimum severity level - all errors over that level will be
   omitted.
1. Enter domain names for which you want to slack logs, each domain must be
   added in new line. It is useful if you don't want to include logging from
   dev, test or stage environment.
1. Click "Save configuration" to save.


## Uninstallation

1. Disable the module from `administration >> extend`.
1. Uninstall the module


## Maintainers

- Mariusz Andrzejewski - [sayco](https://www.drupal.org/u/sayco)
